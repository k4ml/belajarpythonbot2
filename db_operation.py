import os, sys

here = os.path.dirname(os.path.realpath(__file__))
sys.path.append(os.path.join(here, "./vendored"))

import boto3

dynamodb = boto3.resource(
    'dynamodb',
    aws_access_key_id = os.environ['BOTO3_AWS_ACCESS_KEY_ID'],
    aws_secret_access_key = os.environ['BOTO3_AWS_SECRET_ACCESS_KEY'],
)

table = dynamodb.Table(name='BelajarPythonBot')

def add_or_identify_user(user, table=table):
    # takes the user object and if the user is in the db, returns the information about user from db
    # if the user is not in the db, push user info to db and returns the user from db
    try:
        user_db = table.get_item(Key={'UserID':user['id']})["Item"]
        return user_db
    except KeyError:
        if 'username' not in list(user):
            user['username'] = None
        table.put_item(Item={
            'UserID':user['id'], 
            'first_name': user['first_name'], 
            'username': user['username'],
            'question': {'q1':'n','q2':'n','q3':'n','q4':'n'}
        })
        return table.get_item(Key={'UserID':user['id']})["Item"]

def check_user_questions(user, table=table):
    # check how much correct questions answered
    try:
        user_db = table.get_item(Key={'UserID':user['id']})["Item"]
        counter = 0
        for v in user_db['question'].values():
            if v == "c":
                counter = counter + 1
        return counter
        
    except KeyError:
        return "User is not in db"
    
def add_result(user, question, result, table=table):
    try:
        table.update_item(
            Key={'UserID':user['id']},
            UpdateExpression='SET question.#question = :result', 
            ExpressionAttributeNames={'#question': question}, 
            ExpressionAttributeValues={':result': result}
        )
    except KeyError:
        return "User is not in db"